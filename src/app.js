'use strict'

const express = require('express');
const router = express.Router();
const app = express();

const index = router.get('/', (req, res, next) => {
    res.status(200).send({
        title: 'API Rest',
        version: 'v0.0.1'
    });
});

const search = router.get('/:search', (req, res, next) => {
    res.status(200).send({
        search: req.params.search
    });
});

app.use('/', index);
app.use('/search', search);

module.exports = app;